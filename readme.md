# CLI App Readme

This app is an example for running an NPX command app from Node. Run from CLI via `npx git+https://j_goodwin@bitbucket.org/j_goodwin/l3-cliapp.git#v1.0.0`

Alternatively, install globally via

```bash
npm i -g git+https://j_goodwin@bitbucket.org/j_goodwin/l3-cliapp.git#v1.0.0`
```

and then call from the global install via the reference name in the bin section of package.json (sim).

```bash
sim
```
